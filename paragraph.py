import sys

def getAllTerms():
	with open(sys.argv[1], 'r') as content_file:
		paragraph = content_file.read()

	list_terms = paragraph.split()
	terms_hash = {}

	for idx,term in enumerate(list_terms):
		if term not in terms_hash:
			terms_hash[term] = [idx]
		else:
			terms_hash[term].append(idx)

	print terms_hash

getAllTerms()